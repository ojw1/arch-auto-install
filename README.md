# Arch Auto Install


## Create Arch ISO or Use Image
Download ArchISO from [https://archlinux.org/download/](https://archlinux.org/download/)

## Boot Arch ISO
From initial Prompt type the following commands:

```
pacman -Sy git
git clone https://gitlab.com/ojw1/arch-auto-install.git
cd arch-auto-install
./archtitus.sh
```
